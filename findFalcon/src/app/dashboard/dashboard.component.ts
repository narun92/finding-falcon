import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { DataService } from '../data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  form: FormGroup;
  planets: Object;
  vehicles: Object;
  constructor(private formBuilder: FormBuilder,
    private data: DataService) {
    this.form = this.formBuilder.group({
      planets: [''],
      vehicle1: ''
    });
  }

  ngOnInit() {
    // async orders
    this.data.getPlanets().subscribe(planet => {
      this.planets = planet;
      console.log("Planet", this.planets);
    });
  }

  getVehicles() {
    this.data.getVehicles().subscribe(vehicle => {
      this.vehicles = vehicle;
      console.log("Vehicles", this.vehicles);
    });
  }

  submit() {
    // setTimeout(() => {
    console.log(this.form.value);
    // }, 10);
  }

}
